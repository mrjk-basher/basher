#!/usr/bin/env bash
# Summary: Configure the shell environment for basher
# Usage: eval "$(basher init -)"

set -e

shell="$2"
if [ -z "$shell" ]; then
  shell="$(ps -o comm= -o pid= | grep "$PPID" 2>/dev/null || true)"
  shell="${shell##-}"
  shell="${shell%% *}"
  shell="$(basename "${shell:-$SHELL}")"
fi

print_fish_commands() {
  echo "set -gx BASHER_SHELL $shell"
  echo "set -gx BASHER_ROOT $BASHER_ROOT"
  echo "set -gx BASHER_PREFIX $BASHER_PREFIX"
  echo "set -gx BASHER_PACKAGES_PATH $BASHER_PACKAGES_PATH"

  echo 'if not contains $BASHER_PREFIX/bin $PATH'
  echo 'set -gx PATH $BASHER_PREFIX/bin $PATH'
  echo 'end'
}

print_sh_commands(){
  echo "export BASHER_SHELL=$shell"
  echo "export BASHER_ROOT=$BASHER_ROOT"
  echo "export BASHER_PREFIX=$BASHER_PREFIX"
  echo "export BASHER_PACKAGES_PATH=$BASHER_PACKAGES_PATH"

  echo '[[ ":$PATH:" != *":$BASHER_PREFIX/bin:"* ]] && export PATH="$BASHER_PREFIX/bin:$PATH"'

}

load_sh_package_shell() {
  mkdir -p "$BASHER_PREFIX/shell/sh"
  echo 'for f in $(command ls "$BASHER_PREFIX/shell/sh"); do source "$BASHER_PREFIX/shell/sh/$f"; done'
}

load_bash_package_completions() {
  mkdir -p "$BASHER_PREFIX/completions/bash"
  echo 'for f in $(command ls "$BASHER_PREFIX/completions/bash"); do source "$BASHER_PREFIX/completions/bash/$f"; done'
}
load_bash_package_shell() {
  mkdir -p "$BASHER_PREFIX/shell/bash"
  echo 'for f in $(command ls "$BASHER_PREFIX/shell/bash"); do source "$BASHER_PREFIX/shell/bash/$f"; done'
}

load_zsh_package_completions() {
  mkdir -p "$BASHER_PREFIX/completions/zsh/compsys"
  echo 'fpath=("$BASHER_PREFIX/completions/zsh/compsys" $fpath)'
  echo 'for f in $(command ls "$BASHER_PREFIX/completions/zsh/compctl"); do source "$BASHER_PREFIX/completions/zsh/compctl/$f"; done'
}

case "$shell" in
  fish )
    print_fish_commands
    ;;
  bash)
   print_sh_commands
   ;;
  * )
    print_sh_commands
    load_sh_package_shell
    ;;
esac

if [ -e "$BASHER_ROOT/lib/include.$shell" ]; then
  echo ". \"\$BASHER_ROOT/lib/include.$shell\""
fi

if [ -e "$BASHER_ROOT/completions/basher.$shell" ]; then
  echo ". \"\$BASHER_ROOT/completions/basher.$shell\""
fi

case "$shell" in
  bash )
    load_bash_package_shell
    load_bash_package_completions
    ;;
  zsh )
    load_zsh_package_completions
    ;;
esac
